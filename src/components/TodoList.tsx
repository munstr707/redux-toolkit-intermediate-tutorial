import * as React from "react";
import TodoComp from "./Todo";
import { Todo } from "../types";

type Props = {
  todos: Array<Todo>;
  toggleTodo: (id: number) => void;
};

const TodoList: React.FC<Props> = ({ todos, toggleTodo }) => (
  <ul>
    {todos.map(todo => (
      <TodoComp key={todo.id} {...todo} onClick={() => toggleTodo(todo.id)} />
    ))}
  </ul>
);

export default TodoList;
